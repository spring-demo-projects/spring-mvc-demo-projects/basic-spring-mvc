package com.cdac.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cdac.model.Login;

@Controller
public class LoginController {

	/*
	 * Calling this method is job of ServletDispatcher
	 * 
	 * Creating object of Login class and initialize it with email and password
	 * values is also job of Dispatcher Servlet
	 */
	@RequestMapping("/auth")
	public String authenticate(Login l) {
		boolean success = l.getEmail().contains(l.getPassword());
		if (success)
			return "welcome";
		else
			return "failed";
	}

}
